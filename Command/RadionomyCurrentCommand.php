<?php

namespace IiMedias\RadioBundle\Command;

use IiMedias\RadioBundle\Model\Artist;
use IiMedias\RadioBundle\Model\Broadcast;
use IiMedias\RadioBundle\Model\BroadcastQuery;
use IiMedias\RadioBundle\Model\Title;
use IiMedias\RadioBundle\Model\ArtistQuery;
use IiMedias\RadioBundle\Model\RadioQuery;
use IiMedias\RadioBundle\Model\TitleQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use \SimpleXMLElement;
use \DateTime;

class RadionomyCurrentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('radio:radionomy:current')
            ->setDescription('Récupère le titre courant')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jsonReturn = array();

        $radios = RadioQuery::create()
            ->filterByType('radionomy')
            ->create();
        
        foreach ($radios as $radio) {
            $apiCurrentSongUrl    = 'http://api.radionomy.com/currentsong.cfm?radiouid=' . $radio->getRadionomyGuid() . '&apikey=' . $radio->getRadionomyApiKey() . '&callmeback=yes&type=xml&cover=yes';
            $xmlStringCurrentSong = file_get_contents($apiCurrentSongUrl);
            $xmlCurrentSong       = new SimpleXMLElement($xmlStringCurrentSong);

            $artist = ArtistQuery::create()
                ->filterbyName($xmlCurrentSong->track->artists)
                ->findOne();

            if (is_null($artist)) {
                $artist = new Artist();
                $artist
                    ->setName($xmlCurrentSong->track->artists)
                    ->setDisplayName($xmlCurrentSong->track->artists)
                    ->save()
                ;
            }

            $title = TitleQuery::create()
                ->filterByArtist($artist)
                ->filterByName($xmlCurrentSong->track->title)
                ->findOne();

            if (is_null($title)) {
                $title = new Title();
                $title
                    ->setArtist($artist)
                    ->setName($xmlCurrentSong->track->title)
                    ->setDisplayName($xmlCurrentSong->track->title)
                    ->save()
                ;
            }

            $startedAt = new DateTime($xmlCurrentSong->track->starttime);
            $endedAt   = new DateTime();
            $endedAt->setTimestamp($startedAt->getTimestamp() + intval($xmlCurrentSong->track->playduration / 1000));

            $lastBroadcast = BroadcastQuery::create()
                ->filterByRadio($radio)
                ->orderByStartedAt(Criteria::DESC)
                ->findOne()
            ;

            if (is_null($lastBroadcast) || $lastBroadcast->getTitle() != $title) {
                $broadcast = new Broadcast();
                $broadcast
                    ->setRadio($radio)
                    ->setTitle($title)
                    ->setStartedAt($startedAt)
                    ->setEndedAt($endedAt)
                    ->save();
            }
        }

        return json_encode($jsonReturn);
    }
}
