<?php

namespace IiMedias\RadioBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\RadioBundle\Model\Artist as ChildArtist;
use IiMedias\RadioBundle\Model\ArtistQuery as ChildArtistQuery;
use IiMedias\RadioBundle\Model\Broadcast as ChildBroadcast;
use IiMedias\RadioBundle\Model\BroadcastQuery as ChildBroadcastQuery;
use IiMedias\RadioBundle\Model\Title as ChildTitle;
use IiMedias\RadioBundle\Model\TitleQuery as ChildTitleQuery;
use IiMedias\RadioBundle\Model\Map\BroadcastTableMap;
use IiMedias\RadioBundle\Model\Map\TitleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'radio_title_ratitl' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.RadioBundle.Model.Base
 */
abstract class Title implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\RadioBundle\\Model\\Map\\TitleTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the ratitl_id field.
     *
     * @var        int
     */
    protected $ratitl_id;

    /**
     * The value for the ratitl_raarti_id field.
     *
     * @var        int
     */
    protected $ratitl_raarti_id;

    /**
     * The value for the ratitl_name field.
     *
     * @var        string
     */
    protected $ratitl_name;

    /**
     * The value for the ratitl_display_name field.
     *
     * @var        string
     */
    protected $ratitl_display_name;

    /**
     * The value for the ratitl_slug field.
     *
     * @var        string
     */
    protected $ratitl_slug;

    /**
     * @var        ChildArtist
     */
    protected $aArtist;

    /**
     * @var        ObjectCollection|ChildBroadcast[] Collection to store aggregation of ChildBroadcast objects.
     */
    protected $collBroadcasts;
    protected $collBroadcastsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBroadcast[]
     */
    protected $broadcastsScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\RadioBundle\Model\Base\Title object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Title</code> instance.  If
     * <code>obj</code> is an instance of <code>Title</code>, delegates to
     * <code>equals(Title)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Title The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [ratitl_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->ratitl_id;
    }

    /**
     * Get the [ratitl_raarti_id] column value.
     *
     * @return int
     */
    public function getArtistId()
    {
        return $this->ratitl_raarti_id;
    }

    /**
     * Get the [ratitl_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->ratitl_name;
    }

    /**
     * Get the [ratitl_display_name] column value.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->ratitl_display_name;
    }

    /**
     * Get the [ratitl_slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->ratitl_slug;
    }

    /**
     * Set the value of [ratitl_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ratitl_id !== $v) {
            $this->ratitl_id = $v;
            $this->modifiedColumns[TitleTableMap::COL_RATITL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [ratitl_raarti_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function setArtistId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ratitl_raarti_id !== $v) {
            $this->ratitl_raarti_id = $v;
            $this->modifiedColumns[TitleTableMap::COL_RATITL_RAARTI_ID] = true;
        }

        if ($this->aArtist !== null && $this->aArtist->getId() !== $v) {
            $this->aArtist = null;
        }

        return $this;
    } // setArtistId()

    /**
     * Set the value of [ratitl_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ratitl_name !== $v) {
            $this->ratitl_name = $v;
            $this->modifiedColumns[TitleTableMap::COL_RATITL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [ratitl_display_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function setDisplayName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ratitl_display_name !== $v) {
            $this->ratitl_display_name = $v;
            $this->modifiedColumns[TitleTableMap::COL_RATITL_DISPLAY_NAME] = true;
        }

        return $this;
    } // setDisplayName()

    /**
     * Set the value of [ratitl_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ratitl_slug !== $v) {
            $this->ratitl_slug = $v;
            $this->modifiedColumns[TitleTableMap::COL_RATITL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : TitleTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ratitl_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : TitleTableMap::translateFieldName('ArtistId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ratitl_raarti_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : TitleTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ratitl_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : TitleTableMap::translateFieldName('DisplayName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ratitl_display_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : TitleTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ratitl_slug = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 5; // 5 = TitleTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\RadioBundle\\Model\\Title'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aArtist !== null && $this->ratitl_raarti_id !== $this->aArtist->getId()) {
            $this->aArtist = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TitleTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildTitleQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aArtist = null;
            $this->collBroadcasts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Title::setDeleted()
     * @see Title::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TitleTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildTitleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TitleTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TitleTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aArtist !== null) {
                if ($this->aArtist->isModified() || $this->aArtist->isNew()) {
                    $affectedRows += $this->aArtist->save($con);
                }
                $this->setArtist($this->aArtist);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->broadcastsScheduledForDeletion !== null) {
                if (!$this->broadcastsScheduledForDeletion->isEmpty()) {
                    \IiMedias\RadioBundle\Model\BroadcastQuery::create()
                        ->filterByPrimaryKeys($this->broadcastsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->broadcastsScheduledForDeletion = null;
                }
            }

            if ($this->collBroadcasts !== null) {
                foreach ($this->collBroadcasts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[TitleTableMap::COL_RATITL_ID] = true;
        if (null !== $this->ratitl_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TitleTableMap::COL_RATITL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ratitl_id';
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_RAARTI_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ratitl_raarti_id';
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'ratitl_name';
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_DISPLAY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'ratitl_display_name';
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'ratitl_slug';
        }

        $sql = sprintf(
            'INSERT INTO radio_title_ratitl (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ratitl_id':
                        $stmt->bindValue($identifier, $this->ratitl_id, PDO::PARAM_INT);
                        break;
                    case 'ratitl_raarti_id':
                        $stmt->bindValue($identifier, $this->ratitl_raarti_id, PDO::PARAM_INT);
                        break;
                    case 'ratitl_name':
                        $stmt->bindValue($identifier, $this->ratitl_name, PDO::PARAM_STR);
                        break;
                    case 'ratitl_display_name':
                        $stmt->bindValue($identifier, $this->ratitl_display_name, PDO::PARAM_STR);
                        break;
                    case 'ratitl_slug':
                        $stmt->bindValue($identifier, $this->ratitl_slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TitleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getArtistId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getDisplayName();
                break;
            case 4:
                return $this->getSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Title'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Title'][$this->hashCode()] = true;
        $keys = TitleTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getArtistId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getDisplayName(),
            $keys[4] => $this->getSlug(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aArtist) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'artist';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'radio_artist_raarti';
                        break;
                    default:
                        $key = 'Artist';
                }

                $result[$key] = $this->aArtist->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBroadcasts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'broadcasts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'radio_broadcast_rabdcts';
                        break;
                    default:
                        $key = 'Broadcasts';
                }

                $result[$key] = $this->collBroadcasts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\RadioBundle\Model\Title
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TitleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\RadioBundle\Model\Title
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setArtistId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setDisplayName($value);
                break;
            case 4:
                $this->setSlug($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = TitleTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setArtistId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDisplayName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSlug($arr[$keys[4]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TitleTableMap::DATABASE_NAME);

        if ($this->isColumnModified(TitleTableMap::COL_RATITL_ID)) {
            $criteria->add(TitleTableMap::COL_RATITL_ID, $this->ratitl_id);
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_RAARTI_ID)) {
            $criteria->add(TitleTableMap::COL_RATITL_RAARTI_ID, $this->ratitl_raarti_id);
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_NAME)) {
            $criteria->add(TitleTableMap::COL_RATITL_NAME, $this->ratitl_name);
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_DISPLAY_NAME)) {
            $criteria->add(TitleTableMap::COL_RATITL_DISPLAY_NAME, $this->ratitl_display_name);
        }
        if ($this->isColumnModified(TitleTableMap::COL_RATITL_SLUG)) {
            $criteria->add(TitleTableMap::COL_RATITL_SLUG, $this->ratitl_slug);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildTitleQuery::create();
        $criteria->add(TitleTableMap::COL_RATITL_ID, $this->ratitl_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (ratitl_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\RadioBundle\Model\Title (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setArtistId($this->getArtistId());
        $copyObj->setName($this->getName());
        $copyObj->setDisplayName($this->getDisplayName());
        $copyObj->setSlug($this->getSlug());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBroadcasts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBroadcast($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\RadioBundle\Model\Title Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildArtist object.
     *
     * @param  ChildArtist $v
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     * @throws PropelException
     */
    public function setArtist(ChildArtist $v = null)
    {
        if ($v === null) {
            $this->setArtistId(NULL);
        } else {
            $this->setArtistId($v->getId());
        }

        $this->aArtist = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildArtist object, it will not be re-added.
        if ($v !== null) {
            $v->addTitle($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildArtist object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildArtist The associated ChildArtist object.
     * @throws PropelException
     */
    public function getArtist(ConnectionInterface $con = null)
    {
        if ($this->aArtist === null && ($this->ratitl_raarti_id !== null)) {
            $this->aArtist = ChildArtistQuery::create()->findPk($this->ratitl_raarti_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aArtist->addTitles($this);
             */
        }

        return $this->aArtist;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Broadcast' == $relationName) {
            return $this->initBroadcasts();
        }
    }

    /**
     * Clears out the collBroadcasts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBroadcasts()
     */
    public function clearBroadcasts()
    {
        $this->collBroadcasts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBroadcasts collection loaded partially.
     */
    public function resetPartialBroadcasts($v = true)
    {
        $this->collBroadcastsPartial = $v;
    }

    /**
     * Initializes the collBroadcasts collection.
     *
     * By default this just sets the collBroadcasts collection to an empty array (like clearcollBroadcasts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBroadcasts($overrideExisting = true)
    {
        if (null !== $this->collBroadcasts && !$overrideExisting) {
            return;
        }

        $collectionClassName = BroadcastTableMap::getTableMap()->getCollectionClassName();

        $this->collBroadcasts = new $collectionClassName;
        $this->collBroadcasts->setModel('\IiMedias\RadioBundle\Model\Broadcast');
    }

    /**
     * Gets an array of ChildBroadcast objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTitle is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBroadcast[] List of ChildBroadcast objects
     * @throws PropelException
     */
    public function getBroadcasts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBroadcastsPartial && !$this->isNew();
        if (null === $this->collBroadcasts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBroadcasts) {
                // return empty collection
                $this->initBroadcasts();
            } else {
                $collBroadcasts = ChildBroadcastQuery::create(null, $criteria)
                    ->filterByTitle($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBroadcastsPartial && count($collBroadcasts)) {
                        $this->initBroadcasts(false);

                        foreach ($collBroadcasts as $obj) {
                            if (false == $this->collBroadcasts->contains($obj)) {
                                $this->collBroadcasts->append($obj);
                            }
                        }

                        $this->collBroadcastsPartial = true;
                    }

                    return $collBroadcasts;
                }

                if ($partial && $this->collBroadcasts) {
                    foreach ($this->collBroadcasts as $obj) {
                        if ($obj->isNew()) {
                            $collBroadcasts[] = $obj;
                        }
                    }
                }

                $this->collBroadcasts = $collBroadcasts;
                $this->collBroadcastsPartial = false;
            }
        }

        return $this->collBroadcasts;
    }

    /**
     * Sets a collection of ChildBroadcast objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $broadcasts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTitle The current object (for fluent API support)
     */
    public function setBroadcasts(Collection $broadcasts, ConnectionInterface $con = null)
    {
        /** @var ChildBroadcast[] $broadcastsToDelete */
        $broadcastsToDelete = $this->getBroadcasts(new Criteria(), $con)->diff($broadcasts);


        $this->broadcastsScheduledForDeletion = $broadcastsToDelete;

        foreach ($broadcastsToDelete as $broadcastRemoved) {
            $broadcastRemoved->setTitle(null);
        }

        $this->collBroadcasts = null;
        foreach ($broadcasts as $broadcast) {
            $this->addBroadcast($broadcast);
        }

        $this->collBroadcasts = $broadcasts;
        $this->collBroadcastsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Broadcast objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Broadcast objects.
     * @throws PropelException
     */
    public function countBroadcasts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBroadcastsPartial && !$this->isNew();
        if (null === $this->collBroadcasts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBroadcasts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBroadcasts());
            }

            $query = ChildBroadcastQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTitle($this)
                ->count($con);
        }

        return count($this->collBroadcasts);
    }

    /**
     * Method called to associate a ChildBroadcast object to this object
     * through the ChildBroadcast foreign key attribute.
     *
     * @param  ChildBroadcast $l ChildBroadcast
     * @return $this|\IiMedias\RadioBundle\Model\Title The current object (for fluent API support)
     */
    public function addBroadcast(ChildBroadcast $l)
    {
        if ($this->collBroadcasts === null) {
            $this->initBroadcasts();
            $this->collBroadcastsPartial = true;
        }

        if (!$this->collBroadcasts->contains($l)) {
            $this->doAddBroadcast($l);

            if ($this->broadcastsScheduledForDeletion and $this->broadcastsScheduledForDeletion->contains($l)) {
                $this->broadcastsScheduledForDeletion->remove($this->broadcastsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBroadcast $broadcast The ChildBroadcast object to add.
     */
    protected function doAddBroadcast(ChildBroadcast $broadcast)
    {
        $this->collBroadcasts[]= $broadcast;
        $broadcast->setTitle($this);
    }

    /**
     * @param  ChildBroadcast $broadcast The ChildBroadcast object to remove.
     * @return $this|ChildTitle The current object (for fluent API support)
     */
    public function removeBroadcast(ChildBroadcast $broadcast)
    {
        if ($this->getBroadcasts()->contains($broadcast)) {
            $pos = $this->collBroadcasts->search($broadcast);
            $this->collBroadcasts->remove($pos);
            if (null === $this->broadcastsScheduledForDeletion) {
                $this->broadcastsScheduledForDeletion = clone $this->collBroadcasts;
                $this->broadcastsScheduledForDeletion->clear();
            }
            $this->broadcastsScheduledForDeletion[]= clone $broadcast;
            $broadcast->setTitle(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Title is new, it will return
     * an empty collection; or if this Title has previously
     * been saved, it will retrieve related Broadcasts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Title.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBroadcast[] List of ChildBroadcast objects
     */
    public function getBroadcastsJoinRadio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBroadcastQuery::create(null, $criteria);
        $query->joinWith('Radio', $joinBehavior);

        return $this->getBroadcasts($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aArtist) {
            $this->aArtist->removeTitle($this);
        }
        $this->ratitl_id = null;
        $this->ratitl_raarti_id = null;
        $this->ratitl_name = null;
        $this->ratitl_display_name = null;
        $this->ratitl_slug = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBroadcasts) {
                foreach ($this->collBroadcasts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBroadcasts = null;
        $this->aArtist = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TitleTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
