<?php

namespace IiMedias\RadioBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\RadioBundle\Model\Broadcast as ChildBroadcast;
use IiMedias\RadioBundle\Model\BroadcastQuery as ChildBroadcastQuery;
use IiMedias\RadioBundle\Model\Map\BroadcastTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'radio_broadcast_rabdct' table.
 *
 *
 *
 * @method     ChildBroadcastQuery orderById($order = Criteria::ASC) Order by the rabdct_id column
 * @method     ChildBroadcastQuery orderByRadioId($order = Criteria::ASC) Order by the rabdct_rardio_id column
 * @method     ChildBroadcastQuery orderByTitleId($order = Criteria::ASC) Order by the rabdct_ratitl_id column
 * @method     ChildBroadcastQuery orderByStartedAt($order = Criteria::ASC) Order by the rabdct_started_at column
 * @method     ChildBroadcastQuery orderByEndedAt($order = Criteria::ASC) Order by the rabdct_ended_at column
 *
 * @method     ChildBroadcastQuery groupById() Group by the rabdct_id column
 * @method     ChildBroadcastQuery groupByRadioId() Group by the rabdct_rardio_id column
 * @method     ChildBroadcastQuery groupByTitleId() Group by the rabdct_ratitl_id column
 * @method     ChildBroadcastQuery groupByStartedAt() Group by the rabdct_started_at column
 * @method     ChildBroadcastQuery groupByEndedAt() Group by the rabdct_ended_at column
 *
 * @method     ChildBroadcastQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBroadcastQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBroadcastQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBroadcastQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBroadcastQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBroadcastQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBroadcastQuery leftJoinRadio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Radio relation
 * @method     ChildBroadcastQuery rightJoinRadio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Radio relation
 * @method     ChildBroadcastQuery innerJoinRadio($relationAlias = null) Adds a INNER JOIN clause to the query using the Radio relation
 *
 * @method     ChildBroadcastQuery joinWithRadio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Radio relation
 *
 * @method     ChildBroadcastQuery leftJoinWithRadio() Adds a LEFT JOIN clause and with to the query using the Radio relation
 * @method     ChildBroadcastQuery rightJoinWithRadio() Adds a RIGHT JOIN clause and with to the query using the Radio relation
 * @method     ChildBroadcastQuery innerJoinWithRadio() Adds a INNER JOIN clause and with to the query using the Radio relation
 *
 * @method     ChildBroadcastQuery leftJoinTitle($relationAlias = null) Adds a LEFT JOIN clause to the query using the Title relation
 * @method     ChildBroadcastQuery rightJoinTitle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Title relation
 * @method     ChildBroadcastQuery innerJoinTitle($relationAlias = null) Adds a INNER JOIN clause to the query using the Title relation
 *
 * @method     ChildBroadcastQuery joinWithTitle($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Title relation
 *
 * @method     ChildBroadcastQuery leftJoinWithTitle() Adds a LEFT JOIN clause and with to the query using the Title relation
 * @method     ChildBroadcastQuery rightJoinWithTitle() Adds a RIGHT JOIN clause and with to the query using the Title relation
 * @method     ChildBroadcastQuery innerJoinWithTitle() Adds a INNER JOIN clause and with to the query using the Title relation
 *
 * @method     \IiMedias\RadioBundle\Model\RadioQuery|\IiMedias\RadioBundle\Model\TitleQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBroadcast findOne(ConnectionInterface $con = null) Return the first ChildBroadcast matching the query
 * @method     ChildBroadcast findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBroadcast matching the query, or a new ChildBroadcast object populated from the query conditions when no match is found
 *
 * @method     ChildBroadcast findOneById(int $rabdct_id) Return the first ChildBroadcast filtered by the rabdct_id column
 * @method     ChildBroadcast findOneByRadioId(int $rabdct_rardio_id) Return the first ChildBroadcast filtered by the rabdct_rardio_id column
 * @method     ChildBroadcast findOneByTitleId(int $rabdct_ratitl_id) Return the first ChildBroadcast filtered by the rabdct_ratitl_id column
 * @method     ChildBroadcast findOneByStartedAt(string $rabdct_started_at) Return the first ChildBroadcast filtered by the rabdct_started_at column
 * @method     ChildBroadcast findOneByEndedAt(string $rabdct_ended_at) Return the first ChildBroadcast filtered by the rabdct_ended_at column *

 * @method     ChildBroadcast requirePk($key, ConnectionInterface $con = null) Return the ChildBroadcast by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBroadcast requireOne(ConnectionInterface $con = null) Return the first ChildBroadcast matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBroadcast requireOneById(int $rabdct_id) Return the first ChildBroadcast filtered by the rabdct_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBroadcast requireOneByRadioId(int $rabdct_rardio_id) Return the first ChildBroadcast filtered by the rabdct_rardio_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBroadcast requireOneByTitleId(int $rabdct_ratitl_id) Return the first ChildBroadcast filtered by the rabdct_ratitl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBroadcast requireOneByStartedAt(string $rabdct_started_at) Return the first ChildBroadcast filtered by the rabdct_started_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBroadcast requireOneByEndedAt(string $rabdct_ended_at) Return the first ChildBroadcast filtered by the rabdct_ended_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBroadcast[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBroadcast objects based on current ModelCriteria
 * @method     ChildBroadcast[]|ObjectCollection findById(int $rabdct_id) Return ChildBroadcast objects filtered by the rabdct_id column
 * @method     ChildBroadcast[]|ObjectCollection findByRadioId(int $rabdct_rardio_id) Return ChildBroadcast objects filtered by the rabdct_rardio_id column
 * @method     ChildBroadcast[]|ObjectCollection findByTitleId(int $rabdct_ratitl_id) Return ChildBroadcast objects filtered by the rabdct_ratitl_id column
 * @method     ChildBroadcast[]|ObjectCollection findByStartedAt(string $rabdct_started_at) Return ChildBroadcast objects filtered by the rabdct_started_at column
 * @method     ChildBroadcast[]|ObjectCollection findByEndedAt(string $rabdct_ended_at) Return ChildBroadcast objects filtered by the rabdct_ended_at column
 * @method     ChildBroadcast[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BroadcastQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\RadioBundle\Model\Base\BroadcastQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\RadioBundle\\Model\\Broadcast', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBroadcastQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBroadcastQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBroadcastQuery) {
            return $criteria;
        }
        $query = new ChildBroadcastQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBroadcast|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BroadcastTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BroadcastTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBroadcast A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT rabdct_id, rabdct_rardio_id, rabdct_ratitl_id, rabdct_started_at, rabdct_ended_at FROM radio_broadcast_rabdct WHERE rabdct_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBroadcast $obj */
            $obj = new ChildBroadcast();
            $obj->hydrate($row);
            BroadcastTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBroadcast|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rabdct_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE rabdct_id = 1234
     * $query->filterById(array(12, 34)); // WHERE rabdct_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE rabdct_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the rabdct_rardio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRadioId(1234); // WHERE rabdct_rardio_id = 1234
     * $query->filterByRadioId(array(12, 34)); // WHERE rabdct_rardio_id IN (12, 34)
     * $query->filterByRadioId(array('min' => 12)); // WHERE rabdct_rardio_id > 12
     * </code>
     *
     * @see       filterByRadio()
     *
     * @param     mixed $radioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByRadioId($radioId = null, $comparison = null)
    {
        if (is_array($radioId)) {
            $useMinMax = false;
            if (isset($radioId['min'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RARDIO_ID, $radioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($radioId['max'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RARDIO_ID, $radioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RARDIO_ID, $radioId, $comparison);
    }

    /**
     * Filter the query on the rabdct_ratitl_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleId(1234); // WHERE rabdct_ratitl_id = 1234
     * $query->filterByTitleId(array(12, 34)); // WHERE rabdct_ratitl_id IN (12, 34)
     * $query->filterByTitleId(array('min' => 12)); // WHERE rabdct_ratitl_id > 12
     * </code>
     *
     * @see       filterByTitle()
     *
     * @param     mixed $titleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByTitleId($titleId = null, $comparison = null)
    {
        if (is_array($titleId)) {
            $useMinMax = false;
            if (isset($titleId['min'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RATITL_ID, $titleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($titleId['max'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RATITL_ID, $titleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_RATITL_ID, $titleId, $comparison);
    }

    /**
     * Filter the query on the rabdct_started_at column
     *
     * Example usage:
     * <code>
     * $query->filterByStartedAt('2011-03-14'); // WHERE rabdct_started_at = '2011-03-14'
     * $query->filterByStartedAt('now'); // WHERE rabdct_started_at = '2011-03-14'
     * $query->filterByStartedAt(array('max' => 'yesterday')); // WHERE rabdct_started_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $startedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByStartedAt($startedAt = null, $comparison = null)
    {
        if (is_array($startedAt)) {
            $useMinMax = false;
            if (isset($startedAt['min'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_STARTED_AT, $startedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startedAt['max'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_STARTED_AT, $startedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_STARTED_AT, $startedAt, $comparison);
    }

    /**
     * Filter the query on the rabdct_ended_at column
     *
     * Example usage:
     * <code>
     * $query->filterByEndedAt('2011-03-14'); // WHERE rabdct_ended_at = '2011-03-14'
     * $query->filterByEndedAt('now'); // WHERE rabdct_ended_at = '2011-03-14'
     * $query->filterByEndedAt(array('max' => 'yesterday')); // WHERE rabdct_ended_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $endedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByEndedAt($endedAt = null, $comparison = null)
    {
        if (is_array($endedAt)) {
            $useMinMax = false;
            if (isset($endedAt['min'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ENDED_AT, $endedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endedAt['max'])) {
                $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ENDED_AT, $endedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ENDED_AT, $endedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\RadioBundle\Model\Radio object
     *
     * @param \IiMedias\RadioBundle\Model\Radio|ObjectCollection $radio The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByRadio($radio, $comparison = null)
    {
        if ($radio instanceof \IiMedias\RadioBundle\Model\Radio) {
            return $this
                ->addUsingAlias(BroadcastTableMap::COL_RABDCT_RARDIO_ID, $radio->getId(), $comparison);
        } elseif ($radio instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BroadcastTableMap::COL_RABDCT_RARDIO_ID, $radio->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRadio() only accepts arguments of type \IiMedias\RadioBundle\Model\Radio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Radio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function joinRadio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Radio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Radio');
        }

        return $this;
    }

    /**
     * Use the Radio relation Radio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\RadioBundle\Model\RadioQuery A secondary query class using the current class as primary query
     */
    public function useRadioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRadio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Radio', '\IiMedias\RadioBundle\Model\RadioQuery');
    }

    /**
     * Filter the query by a related \IiMedias\RadioBundle\Model\Title object
     *
     * @param \IiMedias\RadioBundle\Model\Title|ObjectCollection $title The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBroadcastQuery The current query, for fluid interface
     */
    public function filterByTitle($title, $comparison = null)
    {
        if ($title instanceof \IiMedias\RadioBundle\Model\Title) {
            return $this
                ->addUsingAlias(BroadcastTableMap::COL_RABDCT_RATITL_ID, $title->getId(), $comparison);
        } elseif ($title instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BroadcastTableMap::COL_RABDCT_RATITL_ID, $title->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTitle() only accepts arguments of type \IiMedias\RadioBundle\Model\Title or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Title relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function joinTitle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Title');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Title');
        }

        return $this;
    }

    /**
     * Use the Title relation Title object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\RadioBundle\Model\TitleQuery A secondary query class using the current class as primary query
     */
    public function useTitleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTitle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Title', '\IiMedias\RadioBundle\Model\TitleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBroadcast $broadcast Object to remove from the list of results
     *
     * @return $this|ChildBroadcastQuery The current query, for fluid interface
     */
    public function prune($broadcast = null)
    {
        if ($broadcast) {
            $this->addUsingAlias(BroadcastTableMap::COL_RABDCT_ID, $broadcast->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the radio_broadcast_rabdct table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BroadcastTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BroadcastTableMap::clearInstancePool();
            BroadcastTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BroadcastTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BroadcastTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BroadcastTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BroadcastTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BroadcastQuery
