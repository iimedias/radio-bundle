<?php

namespace IiMedias\RadioBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\RadioBundle\Model\Radio as ChildRadio;
use IiMedias\RadioBundle\Model\RadioQuery as ChildRadioQuery;
use IiMedias\RadioBundle\Model\Map\RadioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'radio_radio_rardio' table.
 *
 *
 *
 * @method     ChildRadioQuery orderById($order = Criteria::ASC) Order by the rardio_id column
 * @method     ChildRadioQuery orderByName($order = Criteria::ASC) Order by the rardio_name column
 * @method     ChildRadioQuery orderByType($order = Criteria::ASC) Order by the rardio_type column
 * @method     ChildRadioQuery orderByRadionomyGuid($order = Criteria::ASC) Order by the rardio_radionomy_guid column
 * @method     ChildRadioQuery orderByRadionomyApiKey($order = Criteria::ASC) Order by the rardio_radionomy_api_key column
 * @method     ChildRadioQuery orderByCreatedAt($order = Criteria::ASC) Order by the rardio_created_at column
 * @method     ChildRadioQuery orderByUpdatedAt($order = Criteria::ASC) Order by the rardio_updated_at column
 * @method     ChildRadioQuery orderByRardioSlug($order = Criteria::ASC) Order by the rardio_slug column
 *
 * @method     ChildRadioQuery groupById() Group by the rardio_id column
 * @method     ChildRadioQuery groupByName() Group by the rardio_name column
 * @method     ChildRadioQuery groupByType() Group by the rardio_type column
 * @method     ChildRadioQuery groupByRadionomyGuid() Group by the rardio_radionomy_guid column
 * @method     ChildRadioQuery groupByRadionomyApiKey() Group by the rardio_radionomy_api_key column
 * @method     ChildRadioQuery groupByCreatedAt() Group by the rardio_created_at column
 * @method     ChildRadioQuery groupByUpdatedAt() Group by the rardio_updated_at column
 * @method     ChildRadioQuery groupByRardioSlug() Group by the rardio_slug column
 *
 * @method     ChildRadioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRadioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRadioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRadioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRadioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRadioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRadioQuery leftJoinBroadcast($relationAlias = null) Adds a LEFT JOIN clause to the query using the Broadcast relation
 * @method     ChildRadioQuery rightJoinBroadcast($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Broadcast relation
 * @method     ChildRadioQuery innerJoinBroadcast($relationAlias = null) Adds a INNER JOIN clause to the query using the Broadcast relation
 *
 * @method     ChildRadioQuery joinWithBroadcast($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Broadcast relation
 *
 * @method     ChildRadioQuery leftJoinWithBroadcast() Adds a LEFT JOIN clause and with to the query using the Broadcast relation
 * @method     ChildRadioQuery rightJoinWithBroadcast() Adds a RIGHT JOIN clause and with to the query using the Broadcast relation
 * @method     ChildRadioQuery innerJoinWithBroadcast() Adds a INNER JOIN clause and with to the query using the Broadcast relation
 *
 * @method     \IiMedias\RadioBundle\Model\BroadcastQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRadio findOne(ConnectionInterface $con = null) Return the first ChildRadio matching the query
 * @method     ChildRadio findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRadio matching the query, or a new ChildRadio object populated from the query conditions when no match is found
 *
 * @method     ChildRadio findOneById(int $rardio_id) Return the first ChildRadio filtered by the rardio_id column
 * @method     ChildRadio findOneByName(string $rardio_name) Return the first ChildRadio filtered by the rardio_name column
 * @method     ChildRadio findOneByType(int $rardio_type) Return the first ChildRadio filtered by the rardio_type column
 * @method     ChildRadio findOneByRadionomyGuid(string $rardio_radionomy_guid) Return the first ChildRadio filtered by the rardio_radionomy_guid column
 * @method     ChildRadio findOneByRadionomyApiKey(string $rardio_radionomy_api_key) Return the first ChildRadio filtered by the rardio_radionomy_api_key column
 * @method     ChildRadio findOneByCreatedAt(string $rardio_created_at) Return the first ChildRadio filtered by the rardio_created_at column
 * @method     ChildRadio findOneByUpdatedAt(string $rardio_updated_at) Return the first ChildRadio filtered by the rardio_updated_at column
 * @method     ChildRadio findOneByRardioSlug(string $rardio_slug) Return the first ChildRadio filtered by the rardio_slug column *

 * @method     ChildRadio requirePk($key, ConnectionInterface $con = null) Return the ChildRadio by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOne(ConnectionInterface $con = null) Return the first ChildRadio matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRadio requireOneById(int $rardio_id) Return the first ChildRadio filtered by the rardio_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByName(string $rardio_name) Return the first ChildRadio filtered by the rardio_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByType(int $rardio_type) Return the first ChildRadio filtered by the rardio_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByRadionomyGuid(string $rardio_radionomy_guid) Return the first ChildRadio filtered by the rardio_radionomy_guid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByRadionomyApiKey(string $rardio_radionomy_api_key) Return the first ChildRadio filtered by the rardio_radionomy_api_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByCreatedAt(string $rardio_created_at) Return the first ChildRadio filtered by the rardio_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByUpdatedAt(string $rardio_updated_at) Return the first ChildRadio filtered by the rardio_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRadio requireOneByRardioSlug(string $rardio_slug) Return the first ChildRadio filtered by the rardio_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRadio[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRadio objects based on current ModelCriteria
 * @method     ChildRadio[]|ObjectCollection findById(int $rardio_id) Return ChildRadio objects filtered by the rardio_id column
 * @method     ChildRadio[]|ObjectCollection findByName(string $rardio_name) Return ChildRadio objects filtered by the rardio_name column
 * @method     ChildRadio[]|ObjectCollection findByType(int $rardio_type) Return ChildRadio objects filtered by the rardio_type column
 * @method     ChildRadio[]|ObjectCollection findByRadionomyGuid(string $rardio_radionomy_guid) Return ChildRadio objects filtered by the rardio_radionomy_guid column
 * @method     ChildRadio[]|ObjectCollection findByRadionomyApiKey(string $rardio_radionomy_api_key) Return ChildRadio objects filtered by the rardio_radionomy_api_key column
 * @method     ChildRadio[]|ObjectCollection findByCreatedAt(string $rardio_created_at) Return ChildRadio objects filtered by the rardio_created_at column
 * @method     ChildRadio[]|ObjectCollection findByUpdatedAt(string $rardio_updated_at) Return ChildRadio objects filtered by the rardio_updated_at column
 * @method     ChildRadio[]|ObjectCollection findByRardioSlug(string $rardio_slug) Return ChildRadio objects filtered by the rardio_slug column
 * @method     ChildRadio[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RadioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\RadioBundle\Model\Base\RadioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\RadioBundle\\Model\\Radio', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRadioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRadioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRadioQuery) {
            return $criteria;
        }
        $query = new ChildRadioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRadio|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RadioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RadioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRadio A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT rardio_id, rardio_name, rardio_type, rardio_radionomy_guid, rardio_radionomy_api_key, rardio_created_at, rardio_updated_at, rardio_slug FROM radio_radio_rardio WHERE rardio_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRadio $obj */
            $obj = new ChildRadio();
            $obj->hydrate($row);
            RadioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRadio|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the rardio_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE rardio_id = 1234
     * $query->filterById(array(12, 34)); // WHERE rardio_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE rardio_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the rardio_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE rardio_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE rardio_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the rardio_type column
     *
     * @param     mixed $type The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        $valueSet = RadioTableMap::getValueSet(RadioTableMap::COL_RARDIO_TYPE);
        if (is_scalar($type)) {
            if (!in_array($type, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $type));
            }
            $type = array_search($type, $valueSet);
        } elseif (is_array($type)) {
            $convertedValues = array();
            foreach ($type as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $type = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the rardio_radionomy_guid column
     *
     * Example usage:
     * <code>
     * $query->filterByRadionomyGuid('fooValue');   // WHERE rardio_radionomy_guid = 'fooValue'
     * $query->filterByRadionomyGuid('%fooValue%'); // WHERE rardio_radionomy_guid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $radionomyGuid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByRadionomyGuid($radionomyGuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($radionomyGuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_RADIONOMY_GUID, $radionomyGuid, $comparison);
    }

    /**
     * Filter the query on the rardio_radionomy_api_key column
     *
     * Example usage:
     * <code>
     * $query->filterByRadionomyApiKey('fooValue');   // WHERE rardio_radionomy_api_key = 'fooValue'
     * $query->filterByRadionomyApiKey('%fooValue%'); // WHERE rardio_radionomy_api_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $radionomyApiKey The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByRadionomyApiKey($radionomyApiKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($radionomyApiKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_RADIONOMY_API_KEY, $radionomyApiKey, $comparison);
    }

    /**
     * Filter the query on the rardio_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE rardio_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE rardio_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE rardio_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the rardio_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE rardio_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE rardio_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE rardio_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RadioTableMap::COL_RARDIO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the rardio_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByRardioSlug('fooValue');   // WHERE rardio_slug = 'fooValue'
     * $query->filterByRardioSlug('%fooValue%'); // WHERE rardio_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rardioSlug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterByRardioSlug($rardioSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rardioSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_SLUG, $rardioSlug, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\RadioBundle\Model\Broadcast object
     *
     * @param \IiMedias\RadioBundle\Model\Broadcast|ObjectCollection $broadcast the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRadioQuery The current query, for fluid interface
     */
    public function filterByBroadcast($broadcast, $comparison = null)
    {
        if ($broadcast instanceof \IiMedias\RadioBundle\Model\Broadcast) {
            return $this
                ->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $broadcast->getRadioId(), $comparison);
        } elseif ($broadcast instanceof ObjectCollection) {
            return $this
                ->useBroadcastQuery()
                ->filterByPrimaryKeys($broadcast->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBroadcast() only accepts arguments of type \IiMedias\RadioBundle\Model\Broadcast or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Broadcast relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function joinBroadcast($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Broadcast');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Broadcast');
        }

        return $this;
    }

    /**
     * Use the Broadcast relation Broadcast object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\RadioBundle\Model\BroadcastQuery A secondary query class using the current class as primary query
     */
    public function useBroadcastQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBroadcast($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Broadcast', '\IiMedias\RadioBundle\Model\BroadcastQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRadio $radio Object to remove from the list of results
     *
     * @return $this|ChildRadioQuery The current query, for fluid interface
     */
    public function prune($radio = null)
    {
        if ($radio) {
            $this->addUsingAlias(RadioTableMap::COL_RARDIO_ID, $radio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the radio_radio_rardio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RadioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RadioTableMap::clearInstancePool();
            RadioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RadioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RadioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RadioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RadioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(RadioTableMap::COL_RARDIO_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(RadioTableMap::COL_RARDIO_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(RadioTableMap::COL_RARDIO_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildRadioQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(RadioTableMap::COL_RARDIO_CREATED_AT);
    }

    // sluggable behavior

    /**
     * Filter the query on the slug column
     *
     * @param     string $slug The value to use as filter.
     *
     * @return    $this|ChildRadioQuery The current query, for fluid interface
     */
    public function filterBySlug($slug)
    {
        return $this->addUsingAlias(RadioTableMap::COL_RARDIO_SLUG, $slug, Criteria::EQUAL);
    }

    /**
     * Find one object based on its slug
     *
     * @param     string $slug The value to use as filter.
     * @param     ConnectionInterface $con The optional connection object
     *
     * @return    ChildRadio the result, formatted by the current formatter
     */
    public function findOneBySlug($slug, $con = null)
    {
        return $this->filterBySlug($slug)->findOne($con);
    }

} // RadioQuery
