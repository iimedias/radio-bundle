<?php

namespace IiMedias\RadioBundle\Model;

use IiMedias\RadioBundle\Model\Base\TitleQuery as BaseTitleQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'radio_title_ratitl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TitleQuery extends BaseTitleQuery
{

}
