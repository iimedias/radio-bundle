<?php

namespace IiMedias\RadioBundle\Model;

use IiMedias\RadioBundle\Model\Base\RadioBroadcastRabdct as BaseRadioBroadcastRabdct;

/**
 * Skeleton subclass for representing a row from the 'radio_broadcast_rabdct' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RadioBroadcastRabdct extends BaseRadioBroadcastRabdct
{

}
