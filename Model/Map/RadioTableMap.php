<?php

namespace IiMedias\RadioBundle\Model\Map;

use IiMedias\RadioBundle\Model\Radio;
use IiMedias\RadioBundle\Model\RadioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'radio_radio_rardio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RadioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.RadioBundle.Model.Map.RadioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'radio_radio_rardio';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\RadioBundle\\Model\\Radio';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.RadioBundle.Model.Radio';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the rardio_id field
     */
    const COL_RARDIO_ID = 'radio_radio_rardio.rardio_id';

    /**
     * the column name for the rardio_name field
     */
    const COL_RARDIO_NAME = 'radio_radio_rardio.rardio_name';

    /**
     * the column name for the rardio_type field
     */
    const COL_RARDIO_TYPE = 'radio_radio_rardio.rardio_type';

    /**
     * the column name for the rardio_radionomy_guid field
     */
    const COL_RARDIO_RADIONOMY_GUID = 'radio_radio_rardio.rardio_radionomy_guid';

    /**
     * the column name for the rardio_radionomy_api_key field
     */
    const COL_RARDIO_RADIONOMY_API_KEY = 'radio_radio_rardio.rardio_radionomy_api_key';

    /**
     * the column name for the rardio_created_at field
     */
    const COL_RARDIO_CREATED_AT = 'radio_radio_rardio.rardio_created_at';

    /**
     * the column name for the rardio_updated_at field
     */
    const COL_RARDIO_UPDATED_AT = 'radio_radio_rardio.rardio_updated_at';

    /**
     * the column name for the rardio_slug field
     */
    const COL_RARDIO_SLUG = 'radio_radio_rardio.rardio_slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the rardio_type field */
    const COL_RARDIO_TYPE_NORMAL = 'normal';
    const COL_RARDIO_TYPE_RADIONOMY = 'radionomy';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Type', 'RadionomyGuid', 'RadionomyApiKey', 'CreatedAt', 'UpdatedAt', 'RardioSlug', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'type', 'radionomyGuid', 'radionomyApiKey', 'createdAt', 'updatedAt', 'rardioSlug', ),
        self::TYPE_COLNAME       => array(RadioTableMap::COL_RARDIO_ID, RadioTableMap::COL_RARDIO_NAME, RadioTableMap::COL_RARDIO_TYPE, RadioTableMap::COL_RARDIO_RADIONOMY_GUID, RadioTableMap::COL_RARDIO_RADIONOMY_API_KEY, RadioTableMap::COL_RARDIO_CREATED_AT, RadioTableMap::COL_RARDIO_UPDATED_AT, RadioTableMap::COL_RARDIO_SLUG, ),
        self::TYPE_FIELDNAME     => array('rardio_id', 'rardio_name', 'rardio_type', 'rardio_radionomy_guid', 'rardio_radionomy_api_key', 'rardio_created_at', 'rardio_updated_at', 'rardio_slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Type' => 2, 'RadionomyGuid' => 3, 'RadionomyApiKey' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6, 'RardioSlug' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'type' => 2, 'radionomyGuid' => 3, 'radionomyApiKey' => 4, 'createdAt' => 5, 'updatedAt' => 6, 'rardioSlug' => 7, ),
        self::TYPE_COLNAME       => array(RadioTableMap::COL_RARDIO_ID => 0, RadioTableMap::COL_RARDIO_NAME => 1, RadioTableMap::COL_RARDIO_TYPE => 2, RadioTableMap::COL_RARDIO_RADIONOMY_GUID => 3, RadioTableMap::COL_RARDIO_RADIONOMY_API_KEY => 4, RadioTableMap::COL_RARDIO_CREATED_AT => 5, RadioTableMap::COL_RARDIO_UPDATED_AT => 6, RadioTableMap::COL_RARDIO_SLUG => 7, ),
        self::TYPE_FIELDNAME     => array('rardio_id' => 0, 'rardio_name' => 1, 'rardio_type' => 2, 'rardio_radionomy_guid' => 3, 'rardio_radionomy_api_key' => 4, 'rardio_created_at' => 5, 'rardio_updated_at' => 6, 'rardio_slug' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                RadioTableMap::COL_RARDIO_TYPE => array(
                            self::COL_RARDIO_TYPE_NORMAL,
            self::COL_RARDIO_TYPE_RADIONOMY,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('radio_radio_rardio');
        $this->setPhpName('Radio');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\RadioBundle\\Model\\Radio');
        $this->setPackage('src.IiMedias.RadioBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('rardio_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('rardio_name', 'Name', 'VARCHAR', true, 255, null);
        $this->getColumn('rardio_name')->setPrimaryString(true);
        $this->addColumn('rardio_type', 'Type', 'ENUM', true, null, null);
        $this->getColumn('rardio_type')->setValueSet(array (
  0 => 'normal',
  1 => 'radionomy',
));
        $this->addColumn('rardio_radionomy_guid', 'RadionomyGuid', 'VARCHAR', false, 255, null);
        $this->addColumn('rardio_radionomy_api_key', 'RadionomyApiKey', 'VARCHAR', false, 255, null);
        $this->addColumn('rardio_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('rardio_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('rardio_slug', 'RardioSlug', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Broadcast', '\\IiMedias\\RadioBundle\\Model\\Broadcast', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':rabdct_rardio_id',
    1 => ':rardio_id',
  ),
), 'CASCADE', 'CASCADE', 'Broadcasts', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'rardio_created_at', 'update_column' => 'rardio_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'sluggable' => array('slug_column' => 'rardio_slug', 'slug_pattern' => '', 'replace_pattern' => '/\W+/', 'replacement' => '-', 'separator' => '-', 'permanent' => 'false', 'scope_column' => '', 'unique_constraint' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to radio_radio_rardio     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        BroadcastTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RadioTableMap::CLASS_DEFAULT : RadioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Radio object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RadioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RadioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RadioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RadioTableMap::OM_CLASS;
            /** @var Radio $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RadioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RadioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RadioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Radio $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RadioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_ID);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_NAME);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_TYPE);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_RADIONOMY_GUID);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_RADIONOMY_API_KEY);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_CREATED_AT);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_UPDATED_AT);
            $criteria->addSelectColumn(RadioTableMap::COL_RARDIO_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.rardio_id');
            $criteria->addSelectColumn($alias . '.rardio_name');
            $criteria->addSelectColumn($alias . '.rardio_type');
            $criteria->addSelectColumn($alias . '.rardio_radionomy_guid');
            $criteria->addSelectColumn($alias . '.rardio_radionomy_api_key');
            $criteria->addSelectColumn($alias . '.rardio_created_at');
            $criteria->addSelectColumn($alias . '.rardio_updated_at');
            $criteria->addSelectColumn($alias . '.rardio_slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RadioTableMap::DATABASE_NAME)->getTable(RadioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RadioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RadioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RadioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Radio or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Radio object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RadioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\RadioBundle\Model\Radio) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RadioTableMap::DATABASE_NAME);
            $criteria->add(RadioTableMap::COL_RARDIO_ID, (array) $values, Criteria::IN);
        }

        $query = RadioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RadioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RadioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the radio_radio_rardio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RadioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Radio or Criteria object.
     *
     * @param mixed               $criteria Criteria or Radio object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RadioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Radio object
        }

        if ($criteria->containsKey(RadioTableMap::COL_RARDIO_ID) && $criteria->keyContainsValue(RadioTableMap::COL_RARDIO_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RadioTableMap::COL_RARDIO_ID.')');
        }


        // Set the correct dbName
        $query = RadioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RadioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RadioTableMap::buildTableMap();
