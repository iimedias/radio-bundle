<?php

namespace IiMedias\RadioBundle\Model;

use IiMedias\RadioBundle\Model\Base\BroadcastQuery as BaseBroadcastQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'radio_broadcast_rabdct' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BroadcastQuery extends BaseBroadcastQuery
{

}
