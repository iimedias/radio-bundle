<?php

namespace IiMedias\RadioBundle\Model;

use IiMedias\RadioBundle\Model\Base\Title as BaseTitle;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'radio_title_ratitl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Title extends BaseTitle
{
    protected function slugify() {
        dump($this->getDisplayName());

        $slug = $this->getDisplayName();
        $slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        $slug = preg_replace('~[^-\w]+~', '', $slug);
        $slug = trim($slug, '-');
        $slug = preg_replace('~-+~', '-', $slug);
        $slug = strtolower($slug);

        $slugArr = explode('-', $slug);
        for ($i = 0, $iMax = count($slugArr); $i < $iMax; $i++) {
            if ($slugArr[$i] == '') {
                unset($slugArr[$i]);
            }
        }
        $slug = implode('-', $slugArr);

        if (empty($slug)) {
            $slug = 'n-a';
        }

        $this->setSlug($slug);
        dump($slug);

        return $this;
    }

    public function preSave(ConnectionInterface $con = null)
    {
        if (in_array('radio_title_ratitl.ratitl_display_name', $this->getModifiedColumns())) {
            $this->slugify();
        }
        return $this;
    }
}
