<?php

namespace IiMedias\RadioBundle\Model;

use IiMedias\RadioBundle\Model\Base\Radio as BaseRadio;

/**
 * Skeleton subclass for representing a row from the 'radio_radio_rardio' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Radio extends BaseRadio
{

}
